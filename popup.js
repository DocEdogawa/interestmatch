//So inefficient :D
function getFeaturesOfUrl(url){
    //create a new element link with your link
    var a = document.createElement("a");
    a.href=url;
    a.style.display="none";
    document.body.appendChild(a);

    var features = {};
    features.protocol = a.protocol;
    features.hostname = a.hostname;
    features.pathname = a.pathname;
    features.port = a.port;
    features.hash = a.hash;
    //remove it
    document.body.removeChild(a);
    return features;
}

var URL_LIMIT = 20;

function computeVisitedMap(data){
    var visitedWebsites = {};
    data.forEach(function(page) {
        var url = getFeaturesOfUrl(page.url).hostname;
        if(url in visitedWebsites){
            visitedWebsites[url]++;
        }else{
            visitedWebsites[url] = 1;
        }
    });
    return visitedWebsites;
}

function drawVisitedTable(visitedWebsites){
    var tableBody = document.getElementById("visitTableBody");
    var visitedSorted = Object.keys(visitedWebsites).sort(function(a,b){return visitedWebsites[b]-visitedWebsites[a]})
    for(var i = 0; i < visitedSorted.length && i < URL_LIMIT; i++){
        var tr = document.createElement("tr");
        var url = document.createElement("td");
        url.textContent = visitedSorted[i];
        var visitCount = document.createElement("td");
        visitCount.textContent = visitedWebsites[visitedSorted[i]];
        tr.appendChild(url);
        tr.appendChild(visitCount);
        tableBody.appendChild(tr);
    }
}

var bkg;
document.addEventListener('DOMContentLoaded', function() {
    bkg = chrome.extension.getBackgroundPage();
    var totalCount = 10000;
    var visitedWebsitesA = {};
    var visitedWebsitesB = {};
    chrome.history.search({text: '', startTime:0, endTime:Date.now() , maxResults: totalCount}, function(data) {
        visitedWebsitesA = computeVisitedMap(data);
        drawVisitedTable(visitedWebsitesA);
    });
    var twoMonthsAgo = 2*31*24*60*80*1000;
    chrome.history.search({text: '', startTime:0, endTime:Date.now() - twoMonthsAgo, maxResults: totalCount}, function(data) {
        visitedWebsitesB = computeVisitedMap(data);
        var score = computeMatchScore(visitedWebsitesA, visitedWebsitesB, totalCount);
        document.getElementById("matchP").textContent = score+"%";
    });
});

//computes match score in percent
function computeMatchScore(visitedWebsitesA, visitedWebsitesB, totalCount){
    var matchScore = 0;
    bkg.console.log("Compute MS");
    for(const key of Object.keys(visitedWebsitesA)){
        if(visitedWebsitesB[key] != null){
            bkg.console.log("In FOR", key);
            var countA = visitedWebsitesA[key];
            var countB = visitedWebsitesB[key];
            bkg.console.log(countA, countB);
            var similarityFactor = 1 - Math.pow(Math.abs(countA - countB)/totalCount, 2);
            matchScore += (countA+countB)/(totalCount*2) * similarityFactor;
        }
    }
    bkg.console.log( matchScore*100);
    return matchScore*100;
}
